package br.com.usp.each.service.cliente.domain;

import javax.xml.bind.annotation.XmlRootElement;

import br.com.usp.each.estacioaki.domain.Estacionamento;
import br.com.usp.each.estacioaki.domain.Veiculo;

@XmlRootElement
public class ReservarVaga {
	private Veiculo veiculo;
	private Estacionamento estacionamento;
	public Veiculo getVeiculo() {
		return veiculo;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public Estacionamento getEstacionamento() {
		return estacionamento;
	}
	public void setEstacionamento(Estacionamento estacionamento) {
		this.estacionamento = estacionamento;
	}
	
	
	
}
