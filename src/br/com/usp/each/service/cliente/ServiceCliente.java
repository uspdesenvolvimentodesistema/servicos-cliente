package br.com.usp.each.service.cliente;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.usp.each.estacioaki.domain.Aluguel;
import br.com.usp.each.estacioaki.domain.Cliente;
import br.com.usp.each.estacioaki.domain.Veiculo;
import br.com.usp.each.estacionaki.cliente.TarefasCliente;
import br.com.usp.each.service.cliente.domain.ReservarVaga;

import com.google.gson.Gson;

@Path("/cliente")
public class ServiceCliente {
	@GET
	@Path("/erico")
	@Produces(MediaType.APPLICATION_JSON)
	public String numeros() {
		Cliente cliente = new Cliente();
		cliente.setNome("Erico");
		cliente.setId(1l);
		new TarefasCliente().login(cliente);
		return "oi";
	}

	private TarefasCliente task = new TarefasCliente();

	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response Login(Cliente cliente) {
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson(task.login(cliente)))
				.build();
	}

	@POST
	@Path("/reservarvaga")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response reservarVaga(ReservarVaga reserva) {
		Gson gson = new Gson();
		return Response
				.status(200)
				.entity(gson.toJson(task.reservarVaga(reserva.getVeiculo(),
						reserva.getEstacionamento()))).build();
	}

	@POST
	@Path("/cancelarvaga")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response cancelarVaga(Aluguel aluguel) {
		Gson gson = new Gson();
		return Response.status(200)
				.entity(gson.toJson(task.cancelarReservar(aluguel))).build();
	}

	@POST
	@Path("/verificarpendenciaveiculo")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response verificarPendenciaVeiculo(Veiculo veiculo) {
		Gson gson = new Gson();
		return Response.status(200)
				.entity(gson.toJson(task.verificarPendencia(veiculo))).build();
	}
	
	@POST
	@Path("/cadastro")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response cadastro(Cliente cliente) {
		Gson gson = new Gson();
		
		return Response.status(200)
				.entity(gson.toJson(task.cadastrarCliente(cliente))).build();
	}
	
	
	@POST
	@Path("/atualizacao")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response editar(Cliente cliente) {
		Gson gson = new Gson();
		
		return Response.status(200)
				.entity(gson.toJson(task.editarCliente(cliente))).build();
	}
	
	@GET
	@Path("/getmarcas")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTodasMarcas() {
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson(task.getMarcas())).build(); 

	}
	
	@POST
	@Path("/clientePorVeiculo")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getClientePorVeiculo(Veiculo veiculo) {
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson(task.getClientePorVeiculo(veiculo))).build(); 

	}
}
